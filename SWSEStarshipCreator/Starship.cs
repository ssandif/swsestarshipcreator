﻿//-----------------------------------------------------------------------
// <copyright file="Starship.cs" company="ssandif">
//     Copyright (c) ssandif. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SWSEStarshipCreator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Saga Edition Starship
    /// </summary>
    public class Starship
    {
        /// <summary>
        /// Gets or sets the modifications applied to the ship
        /// </summary>
        private List<Modification> Modifications { get; set; }
    }
}
