﻿//-----------------------------------------------------------------------
// <copyright file="Weapon.cs" company="ssandif">
//     Copyright (c) ssandif. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SWSEStarshipCreator
{
    /// <summary>
    /// Gets or sets the a starship weapon
    /// </summary>
    public class Weapon : Modification
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Weapon"/> class. 
        /// </summary>
        /// <param name="mod"><see cref="Weapon"/> to copy information from</param>
        /// <param name="gunner">Gunner assigned to the weapon</param>
        /// <param name="attack">Attack modifier</param>
        /// <param name="damage">Damage dealt by the weapon</param>
        /// <param name="damageMultiplier">Multiplier for damage</param>
        public Weapon(
            Modification mod,
            string gunner,
            int attack,
            int damage,
            int damageMultiplier = 1)
            : base(mod)
        {
            this.Gunner = gunner;
            this.Attack = attack;
            this.Damage = damage;
            this.DamageMultiplier = damageMultiplier;
        }

        /// <summary>
        /// Gets or sets the Weapon's assigned gunner
        /// </summary>
        public string Gunner { get; set; }

        /// <summary>
        /// Gets or sets the Weapon's attack bonus
        /// </summary>
        public int Attack { get; set; }

        /// <summary>
        /// Gets or sets the Weapon's damage
        /// </summary>
        public int Damage { get; set; }

        /// <summary>
        /// Gets or sets the Weapon's damage multiplier
        /// </summary>
        public int DamageMultiplier { get; set; }
    }
}
