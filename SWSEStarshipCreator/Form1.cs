﻿//-----------------------------------------------------------------------
// <copyright file="Form1.cs" company="ssandif">
//     Copyright (c) ssandif. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SWSEStarshipCreator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    /// <summary>
    /// Main program window
    /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Form1"/> class. 
        /// </summary>
        public Form1()
        {
            this.InitializeComponent();
        }
    }
}
