﻿//-----------------------------------------------------------------------
// <copyright file="Modification.cs" company="ssandif">
//     Copyright (c) ssandif. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SWSEStarshipCreator
{
    /// <summary>
    /// A starship modification
    /// </summary>
    public class Modification
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Modification" /> class.
        /// </summary>
        /// <param name="name">Name of the <see cref="Modification"/></param>
        /// <param name="points">Number of emplacement points required to install</param>
        /// <param name="availability">Restriction on availability</param>
        /// <param name="cost">Cost to install</param>
        public Modification(string name, int points, string availability, int cost)
        {
            this.Name = name;
            this.Points = points;
            this.Availability = availability;
            this.Cost = cost;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Modification" /> class via copy constructor.
        /// </summary>
        /// <param name="other">Object to copy from</param>
        public Modification(Modification other)
        {
            this.Name = other.Name;
            this.Points = other.Points;
            this.Availability = other.Availability;
            this.Cost = other.Cost;
        }

        /// <summary>
        /// Gets the name of the <see cref="Modification"/>.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the number of emplacement points required to install the <see cref="Modification"/>.
        /// </summary>
        public int Points { get; private set; }

        /// <summary>
        /// Gets the restriction level on the availability of the <see cref="Modification"/>.
        /// </summary>
        public string Availability { get; private set; }

        /// <summary>
        /// Gets the cost, in credits, to install the <see cref="Modification"/>.
        /// </summary>
        public int Cost { get; private set; }
    }
}
